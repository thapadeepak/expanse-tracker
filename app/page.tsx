import NavBar from "@/components/NavBar/Navbar";
const Home = () => {
  return (
    <>
      <NavBar />
      <div>Welcome Home !</div>;
    </>
  );
};
export default Home;
