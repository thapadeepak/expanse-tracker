"use client";
import { Layout, theme, Menu } from "antd";
import AdminHeader from "@/components/AdminNavBar/AdminHeader";
import {
  ContainerOutlined,
  DesktopOutlined,
  PieChartOutlined,
  SlidersOutlined,
  TeamOutlined,
  DeploymentUnitOutlined,
  UnorderedListOutlined,
} from "@ant-design/icons";
import NavLink from "../nav-link";
const { Content, Sider } = Layout;
export default function AdminLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    <Layout>
      {/* <AdminNavBar collapsed={collapsed} /> */}
      <Sider trigger={null}>
        <Menu
          theme="light"
          mode="inline"
          style={{ marginTop: "3rem" }}
          defaultSelectedKeys={["1"]}
          items={[
            {
              key: "1",
              icon: <PieChartOutlined />,
              label: <NavLink href="/">Dashboard</NavLink>,
            },
            {
              key: "2",
              icon: <TeamOutlined />,
              label: <NavLink href="/users">Users</NavLink>,
            },
            {
              key: "3",
              icon: <DeploymentUnitOutlined />,
              label: <NavLink href="/Web">Web</NavLink>,
            },
            {
              key: "4",
              icon: <UnorderedListOutlined />,
              label: <NavLink href="/complaints">Complaints</NavLink>,
            },
            {
              key: "5",
              icon: <SlidersOutlined />,
              label: <NavLink href="/accounts">Account</NavLink>,
            },
          ]}
        />
      </Sider>
      <Layout>
        <AdminHeader />
        <Content
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
}
