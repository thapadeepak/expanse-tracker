"use client";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useRouter } from "next/navigation";
import  axios  from "axios";

const Register = () => {
  const route = useRouter();
  const [user, setUser] = useState({
    username: "",
    email: "",
    password: ""
  });

  const [buttonDisabled, setButtonDisabled] = useState(false);

  const onSignUp = async() => {
    try {
     
      const response= await axios.post("/api/users/signup", user);
      
      console.log("Signup  Success", response.data);
    
      route.push("/Login");
    } catch (error:any) {

      console.log("Singnup failed",error.message)

    }
  };
  useEffect(() => {
    if (
      user.email.length > 0 &&
      user.password.length > 0 &&
      user.username.length > 0
    ) {
      setButtonDisabled(false);
    } else {
      setButtonDisabled(true);
    }
  }, [user]);

  return (
    <div>
      <h1>sign up</h1>

      <label>userName</label>
      <input
        type="text"
        id="username"
        value={user.username}
        onChange={(e) => setUser({ ...user, username: e.target.value })}
        placeholder="username"
      />

      <label>Email</label>
      <input
        type="text"
        id="email"
        value={user.email}
        onChange={(e) => setUser({ ...user, email: e.target.value })}
        placeholder="email"
      />

      <label>Password</label>
      <input
        type="password"
        id="password"
        value={user.password}
        onChange={(e) => setUser({ ...user, password: e.target.value })}
        placeholder="*******"
      />

      <button onClick={onSignUp}>
        {buttonDisabled ? "No signup" : "singup"}
      </button>
    </div>
  );
};

export default Register;
