import { NextResponse } from "next/server";
import type { NextRequest } from "next/server";

// This function can be marked `async` if using `await` inside
export function middleware(request: NextRequest) {
  const path = request.nextUrl.clone().pathname;
  console.log(path)
  const isPublicPath = ["/Login", "/Signup","/"].includes(path); 
  const token = request.cookies.get("token")?.value || "";
  console.log(token);
  if (isPublicPath && token) {
    return NextResponse.redirect(new URL("/Dashboard", request.nextUrl));
  }

  if (!isPublicPath && !token) {
    console.log(request.nextUrl)
    return NextResponse.redirect(new URL("/", request.nextUrl));
  }
}

// See "Matching Paths" below to learn more
export const config = {
  matcher: ["/", "/Login", "/Signup","/Dashboard"],
};
