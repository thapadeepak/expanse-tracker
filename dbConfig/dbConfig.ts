import monooose from "mongoose";


export async function connect() {
  try {
    monooose.connect(process.env.MONGO_URI!);
    const connection = monooose.connection;

    connection.on("connected", () => {
        console.log('MongoDB Connected Successfully');
    });

    connection.on("error",(err)=>{
        console.log("MongoDb Connection Error. Please make sure MongoDb is running "+err);
        process.exit();
    })


  } catch (error) {
    console.log("Something goes wrong");
    console.log(error);
  }
}
