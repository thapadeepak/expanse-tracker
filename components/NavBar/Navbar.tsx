"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";
import logo from "@/app/public/Logo.svg";
import Image from "next/image";
import "./Navbar.css";

const navLink = [
  { name: "Home", href: "/Home" },
  { name: "Features", href: "/Features" },
  { name: "Contact", href: "/Contact" },
];

const Navbar = () => {
  const pathname = usePathname();
  const clickHandler = () => {
    return <Link href="/" />;
  };
  return (
    <header className="header">
      <div className="logo">
        <Image
          src={logo}
          alt="logo"
          height={80}
          width={80}
          onClick={clickHandler}
        />
      </div>
      {/* {showNavBar && ( */}
      <nav>
        <div className="nav-items">
          {navLink.map((link) => {
            const isActive = pathname.startsWith(link.href);
            return (
              <Link
                href={link.href}
                key={link.name}
                className={isActive ? "nav-item " : "nav-item"}
              >
                {link.name}
              </Link>
            );
          })}
        </div>
        <div className="auth-buttons">
          <button type="button" className="btn btn-login">
            <Link key="login" href="/Login">
              sign in
            </Link>
          </button>
          <button>
            <Link key="signup" href="/Signup">
              sign up
            </Link>
          </button>
        </div>
      </nav>
      {/* )} */}
    </header>
  );
};

export default Navbar;
