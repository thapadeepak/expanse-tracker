// "use client";

// import { Layout, Menu } from "antd";
// import type { MenuProps } from "antd";
// import { useState } from "react";
// import {
//   ContainerOutlined,
//   DesktopOutlined,
//   PieChartOutlined,
// } from "@ant-design/icons";

// const { Sider } = Layout;
// const items: MenuItem[] = [
//   getItem("Dashboard", "1", <PieChartOutlined />),
//   getItem("Users", "2", <DesktopOutlined />),
//   getItem("Web Details", "3", <ContainerOutlined />),

// ];

// function getItem(
//   label: React.ReactNode,
//   key: React.Key,
//   icon?: React.ReactNode,
//   children?: MenuItem[],
//   type?: "group",
// ): MenuItem {
//   return {
//     key,
//     icon,
//     children,
//     label,
//     type,
//   } as MenuItem;
// }

// type MenuItem = Required<MenuProps>["items"][number];

// const AdminNavBar = (props: { collapsed: boolean }) => {
//   return (
//     <>
//       <Sider trigger={null} collapsible collapsed={props.collapsed}>
//         <div className="demo-logo-vertical" />
//         <Menu 
//           defaultSelectedKeys={["1"]}
//           defaultOpenKeys={["sub1"]}
//           mode="inline"
//           theme="light"
//           inlineCollapsed={props.collapsed}
//           items={items}
//         />
//       </Sider>
//     </>
//   );
// };

// export default AdminNavBar;
