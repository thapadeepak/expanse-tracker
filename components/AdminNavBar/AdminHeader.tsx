"use client";
import { Layout, Button, Menu } from "antd";
import { MenuUnfoldOutlined, MenuFoldOutlined } from "@ant-design/icons";
import { UserOutlined } from "@ant-design/icons";
import { Avatar } from "antd";
import axios from "axios";
import { useRouter } from "next/navigation";

const { Header } = Layout;

const AdminHeader = () => {
  const router = useRouter();

  const logout = async () => {
    try {
      await axios.get("/api/users/logout");
      console.log("logout successful");
      router.push("/Login");
    } catch (error: any) {
      console.log("Error on logout");
      console.log(error.message);
    }
  };
  return (
    <Header
      style={{ display: "flex", alignItems: "center", justifyContent: "end" }}
    >
      <div onClick={logout} 
      style={{cursor:"pointer"}}>
        <Avatar icon={<UserOutlined />} />
        <span style={{ color: "white" }}>Deepak</span>
      </div>
    </Header>
  );
};

export default AdminHeader;
